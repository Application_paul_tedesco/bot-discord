module.exports = {
    name: 'ping',
    aliases: 'pg',
	description: 'Ping!',
	cooldown: 5,
	execute(message) {
		message.channel.send('Pong.');
	},
};